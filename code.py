# code.py (LumosRing Clock example)
# Copyright: 2022 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython program demonstrates the basics of the LumosRing LEDs, buttons, and tone buzzer.

The LumosRing hardware connects via USB and provides:
    - 310 Neopixel LEDs
        - ring of 240 LEDs as 60 lines of 4 LEDs each
        - block of 10x7 LEDs as 7 lines of 10 LEDs each
    - tone buzzer
    - 2 buttons
    - a small mass storage device (used to store the code and related files)

The LumosRing is powered by a Lolin ESP32-S2 which include Wifi connectivity.

The sample code used the Adafruit IO API to synchronize a analog-style LED clock.
For a little fun, it animates a pair of eyes in the LumosRing block.

Documentation: 
**Hardware:**
    * the LumosRing (available on Tindie)
      https://www.tindie.com/stores/bradanlane/
"""

import supervisor
import random
import time

try:
    from LumosRing import LumosRing, SimpleTimer   # REQUIRED: import the LumosRing library
except ImportError as e:
    print("------------------------------------------------------------------------")
    print(e)
    print("------------------------------------------------------------------------")
    print ("Error: Missing 'lib/LumosRing.py'")
    print ("The latest LumosRing library is available from:")
    print ("https://gitlab.com/bradanlane/circuitpython-projects/-/tree/main/LumosRing")
    print("------------------------------------------------------------------------")


class Eyes:
    """
        The Eyes class is a fun example to display a pair of custom glyphs in the 
        center block of LEDs of the LumosRing
    """
    FRONT = 0
    LEFT = 1
    RIGHT = 2
    BLINK0 = 3
    BLINK1 = 4
    BLINK2 = 5
    BLINK3 = 6
    BLINK4 = 7
    BLINK5 = 8
    BLINK6 = 9

    def __init__(self, ring, color):
        self.lumos = ring
        self.position = 0
        self.color = color

        # bitmaps for the various eye appearances
        self.eye_front  = [0b01110, 0b10001, 0b10101, 0b10101, 0b10001, 0b01110, 0b00000]
        self.eye_right  = [0b01110, 0b10001, 0b10011, 0b10011, 0b10001, 0b01110, 0b00000]
        self.eye_left   = [0b01110, 0b10001, 0b11001, 0b11001, 0b10001, 0b01110, 0b00000]
        self.eye_blink0 = [0b00000, 0b01110, 0b10001, 0b10101, 0b10001, 0b01110, 0b00000]
        self.eye_blink1 = [0b00000, 0b00000, 0b01110, 0b10101, 0b10001, 0b01110, 0b00000]
        self.eye_blink2 = [0b00000, 0b00000, 0b01110, 0b10101, 0b11111, 0b00000, 0b00000]
        self.eye_blink3 = [0b00000, 0b00000, 0b00000, 0b01110, 0b11111, 0b00000, 0b00000]

        self.eye_frames = [self.eye_front, self.eye_left, self.eye_right, self.eye_blink0, self.eye_blink1, self.eye_blink2, self.eye_blink3, self.eye_blink2, self.eye_blink1, self.eye_blink0]
        self.eyes = [0,0,0,0,0,0,0]

        self.timer = SimpleTimer(500)
        self.timer.start()

    def make(self, left, right):
        lframe = self.eye_frames[left]
        rframe = self.eye_frames[right]
        for i in range(7):
            self.eyes[i] = (lframe[i] << 5) + rframe[i]

    def update(self):
        if self.timer.expired:
            new_position = -1
            inc = 0
            if self.position == Eyes.FRONT:
                new_position = self.position
                while new_position == self.position:
                    new_position = random.randint(0, 4)
                self.position = new_position
                if self.position >= Eyes.BLINK0:
                    inc = 33
            else:
                if self.position >= Eyes.BLINK0:
                    inc = 50
                    if self.position < Eyes.BLINK6:
                        self.position += 1
                    else:
                        self.position = Eyes.FRONT
                else:
                    self.position = Eyes.FRONT

            if not inc:
                inc = random.randint(500, 2500)

            self.make(self.position, self.position)
            self.lumos.bitmap(self.eyes, self.color)
            self.timer.interval = inc
            self.timer.start()

lumos = LumosRing(mode=LumosRing.CLOCK)
lumos.markers = True
eyes = Eyes(lumos, color=0x005f1f) # fun animated eyes

while True:
    # we randomize eye movement (front is always between other positions)
    eyes.update()
    lumos.update()

    while lumos.buttons_changed:
        button, action = lumos.button
        if (button == LumosRing.RIGHT) and (action == LumosRing.PRESSED):
            lumos.brightness += 0.05
        if (button == LumosRing.LEFT) and (action == LumosRing.PRESSED):
            lumos.brightness -= 0.05
# end of while True loop
