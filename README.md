# LumosRing

<img align="right" style="padding: 0 0 0 12px;" src="files/lumosring_512.jpg"/>

The LumosRing is a programmable neopixel 'display' consisting of a matrix ring of 240 LEds (4x60) and a grid block of 70 LEDs (10x7).
It also has a tone buzzer and a pair of buttons to add interactions.

The LumosRing is a USB device running a CircuitPython program.

Plug the LumosRing into a computer and the sample program creates an internet enabled clock.

But that is just the beginning. The LumosRing is customizable and programmable so it can be used for almost anything you can imagine! There are several samples, including an animation. a metronome, and a chess clock. A LumosRing project may be programmed using any of the existing CircuitPython neopixel libraries. there is also a LumosRing API to simplify time-based projects like clocks, timers, and reaction games.

The LumosRing behavior and features are totally customizable using CircuitPython - an easy to use programming language. And if you really don't want to learn to program, that's OK too. The LumosRing is preprogrammed as a clock and there are other samples you can simply drag-and-drop to run.

If you are already familiar with the LumosRing and want to really customize it then take a look at the [**API**](API.md) for all the configuration and capabilities.

[[_TOC_]] 


## Hardware Under the Hood

The LumosRing incorporates the an ESP32-S2 to control the neopixel leds, tone buzzer, buttons, and provide wifi connectivity.

In the sample `code.py` displays an analog-style clock with hours, minutes, and seconds. It adds a traditional set of marking around the perimeter. It also adds fun eyes which randomly look around and blink.

The clock sample uses the buttons to manually set the time if internet connectivity has not been configured. The buttons are also used to adjust the brightness of the clock.

The LumosRing is [available](https://www.tindie.com/stores/bradanlane/) fully assembled. There are options for a special black LED acrylic face kit and for external arcade buttons. _(as shown in picture)_

## Software

The LumosRing runs [CircuitPython](https://circuitpython.org/) - a programming language designed to be easy to learn and run on low-cost hardware devices.

The LumosRing comes with all the necessary software pre-installed. It's software may be upgraded at anytime. The necessary files include:
- [CircuitPython for the Lolin ESP32-S2](https://circuitpython.org/board/lolin_s2_mini/)
- Libraries from the [Adafruit CircuitPython Bundle](https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases)
- [The LumosRing library, fonts, and sample code.py](https://gitlab.com/bradanlane/circuitpython-projects/-/tree/main/lumosring)


### Getting Started with CircuitPython

If you are new to programming CircuitPython or not familiar with programming at all, then 
a great place to start is [Welcome to CircuitPython](https://learn.adafruit.com/welcome-to-circuitpython) from Adafruit.

CircuitPython is easy to learn or just to make small changes.
You edit the `code.py` file with any text editor _such as **Notepad** (Windows) or **TextEdit** (Mac) or **Nano** (Linux)_ or you can use 
an editor like [**Mu**](https://learn.adafruit.com/welcome-to-circuitpython/installing-mu-editor) which has special features to make CircuitPython programming even easier.
You can read more about editing code for CircuitPython in [Adafruit's guide](https://learn.adafruit.com/welcome-to-circuitpython/creating-and-editing-code).

If you are new to CircuitPython, it's important to know that indentations are important. CircuitPython - _like Python_ - uses indentation to organize code
and code blocks. The recommended method of indenting is to always use spaces. Editors like **Mu** make this easy.

### Flow of the Code

Here is a typical CircuitPython LumosRing program, when using the LumosRing API ...

<img align="center" style="width: 90%" src="files/flow_diagram.png"/>

The **setup** is where we create our LumosRing object and establish the initial behavior. Some things to setup now are the type of usage (Clock, Timer, CountDown, non of the above), colors, and any customization to the behavior.

Now the program will **begin** a _loop_ which will run forever. Everything the LumosRing will do, it will be inside this loop.

>>>
Remember that note above about indentation? The instructions within the loop are identified by their level of indentation. Similar, where there is an _if_ statement or another loop, the code statements for them are indented again. The examples below will give you a chance to work with the code and become familiar how CircuitPython relies on indentation to group code statements.
>>>

Inside the **begin** loop the program it needs to call _update()_ to give the LumosRing an opportunity to perform periodic tasks like getting updating the time (when it's being used as a clock), checking the buttons, and updating the LEDs.

Now that the LumosRing has been updated, there is another _loop_ to let the program take action for _buttons_changed_ - eg if either the left or right button has been pressed or released since the last time they were checked.

Inside this loop the code gets a changed button and works with it. In the clock sample `code.py` the buttons change the brightness of the LEDs. In the Chess Timer sample, the buttons switch between players during a match.

The end of the indentation level for _buttons_changed_ loop causes the execution to go back to the start of the _buttons_changes_ loop and look for more changes. Once all of the button presses and releases have been processed, this loop ends and the execution goes back to _begin_.

Here is what this looks like in CircuitPython ...

```python
# SETUP
lumos = LumosRing(mode=LumosRing.CLOCK)
lumos.markers = True
eyes = Eyes(lumos, color=0x005f1f) # fun animated eyes

#BEGIN
while True:
    #UPDATE
    eyes.update()
    lumos.update()

    # BUTTONS
    while lumos.buttons_changed:
        button, action = lumos.button
        if (button == LumosRing.RIGHT) and (action == LumosRing.PRESSED):
            lumos.brightness += 0.05
        if (button == LumosRing.LEFT) and (action == LumosRing.PRESSED):
            lumos.brightness -= 0.05
# END loop
```

## Learn to Customize

The `code.py` from this project provides an easy starting point for any customization you may want.

When you plug the LumosRing into the computer USB There will a new USB storage device called `CIRCUITPY`. This device is where the clock sample `code.py` file is.

The LumosRing runs [CircuitPython][circuitpython] which is an easy and approachable programming language. No special tools are needed.

Browse the `CIRCUITPY` device. You will see something that looks like this ...
```
Directory of CIRCUITPY:
├── font
│   ├── 5x7.pcf
├── lib
│   ├── adafruit_bitmap_font
│   │   ├── **
│   ├── adafruit_led_animation
│   │   ├── **
│   ├── adafruit_datetime.mpy
│   ├── adafruit_requests.mpy
│   ├── adafruit_ntp.mpy
│   ├── lumosring.py
├── code.py
├── secrets.py
```

**UPDATE 14-September-2022:** the `adafruit_ntp.mpy` was added for NTP internet server support. If this library has not been installed, the internal `resync()` method will skip checking NTP.


### Example: Configuring WiFi

The `secrets.py` contains the connectivity information for wifi access. If you want to have the LumosRing Clock sample automatically get the current time from the internet, then edit this file.

Open `secrets.py` in your text editor.

The contents should look like this ...

``` JSON
secrets = {
    'ssid' : 'your_wifi_ssid',
    'password' : 'your_wifi_password',
}
```

The `ssid` and `password` are the settings for your own WiFi.


### Example: Configuring AdafruitIO

The `secrets.py` contains the connectivity information for Adafruit's IO services. If you want to have the LumosRing Clock sample automatically get the current time from the internet, then edit this file and add the `aio_username`, `aio_key`, and `timezone` data.

Open `secrets.py` in your text editor.

The contents should look like this ...

``` JSON
secrets = {
    'ssid' : 'your_wifi_ssid',
    'password' : 'your_wifi_password',
    'aio_username' : 'your_aio_username',
    'aio_key' : 'your_aio_keystring',
    'timezone' : 'America/New_York', 
}
```

The `aio_*` settings are for Adafruit's IO services. These services are very easy to use.
Take a look at the [Adafruit IO](https://io.adafruit.com) website. If you do not yet have an account and would like to use the service to get the time, then create an account and then create a key.


### Example: Configuring WorldTimeAPI

The `secrets.py` contains the connectivity information for WorldTimeAPI. If you want to have the LumosStick Clock sample automatically get the current time from the internet using this service, then edit this file and add the `timezone` data.

Open `secrets.py` in your text editor.

The contents should look like this ...

``` JSON
secrets = {
    'ssid' : 'your_wifi_ssid',
    'password' : 'your_wifi_password',
    'timezone' : 'America/New_York', 
}
```

The timezone is a text string and may be found from the [WorldTimeAPI website] (http://worldtimeapi.org/timezones).


### Example: Configuring NTP

If WiFi is available and AdafruiIO settings to not exist is the `secrets.py` file, the LumosRing will attempt to access an NTP server.

Open `secrets.py` in your text editor.

The contents should look like this ...

``` JSON
secrets = {
    'ssid' : 'your_wifi_ssid',
    'password' : 'your_wifi_password',
    'ntp_server' : 'pool.ntp.org',
    'ntp_offset' : 0,
}
```

The `ntp_server` will default to `pool.ntp.org` if it is not provided. The `ntp_offset` is an integer value representing the timezone offset from UTC and it will default to 0 (zero) if not present. If there is WiFi connectivity, and not NTP settings are provided, the LumosRing will attempt to connect to NTP and set the time to UTC.


### Example: Changing colors

The clock sample uses the default colors for the 'hands' with red for hours, green for minutes, and blue for seconds.

The `code.py` file is where you will make all of your changes. Start by copying the file to a safe place _(if we make a mistake, you can just copy this safe version back to `CIRCUITPY`.)_

Now, open `code.py` in your text editor.

Let's go ahead and set our own colors for the clock. There is a background color and the colors for yours, minutes, seconds, ticks*, and the markers. _(The ticks are fun to watch for a little while but probably are more interesting for games.)_

In the `code.py` you should see some code that looks like this (it's at the bottom):

```python
lumos = LumosRing(mode=LumosRing.CLOCK)
lumos.markers = True
eyes = Eyes(lumos, color=0x005f1f) # fun animated eyes
```

This is the place where we setup the LumosRing API. Let's start by setting the background color for the clock.

Modify this code so that it looks like this:

```python
lumos = LumosRing(mode=LumosRing.CLOCK)
lumos.markers = True
lumos.clock_background = LumosRing.RED
eyes = Eyes(lumos, color=0x005f1f) # fun animated eyes
```

**Note:** CircuitPython is _whitespace-sensitive_. That means it pays attention to indentation. To be safe, always use spaces (not tabs) to indent lines. Fortunately, there is no indentation to `key_sequences`. But, you will see it in the last example.

Back to our example ...

Save the file! The LumosRing should restart automatically.

That red background is rather bright! We can tone it down a bit.

Colors are defined using **HEX Values**. A color starts with '0x' and then there is a two digit hexadecimal value for red, green, and blue respectively. _(you see an example of a hexadecimal color being used for the eyes)_. the predefined color, LumosRing.RED is the same as 0xFF0000 which means full read, no green, and no blue. Let's try a much lower value for red.

Modify this code so that it looks like this:

```python
lumos = LumosRing(mode=LumosRing.CLOCK)
lumos.markers = True
lumos.clock_background = 0x0F0000
eyes = Eyes(lumos, color=0x005f1f) # fun animated eyes
```

Save the file! The LumosRing should restart automatically. The background is definitely less intense. If you don't want a background color, either remove the line or set the clock_background to LumosRing.BLACK which is 0x000000.

Now let's try changing the colors of the hands and the markers.

Starting with the same lines of code, add a new line so that it looks like this:

```python
lumos = LumosRing(mode=LumosRing.CLOCK)
lumos.markers = True
lumos.clock_background = LumosRing.BLACK
lumos.clock_colors = [0x003F7F, 0x001F3F, 0x5F3F00, 0x1F1F1F, 0x1F0F00]
eyes = Eyes(lumos, color=0x005f1f) # fun animated eyes
```

The clocks colors will now be light blue for the hours, medium blue for the minutes, yellow for the seconds, and orange for the markers.

Save the file! The LumosRing should restart automatically and then see the new colors*.

_*the new colors take affect only after the clock has been set_


### Example: Make the Clock Tick

Do you want to clock sample to make a ticking sound? Perhaps not, but let's look at how to do it anyway.

Here is part of the original code ...

``` Python
while True:
    # we randomize eye movement (front is always between other positions)
    eyes.update()
    lumos.update()
```

To make a ticking sound each second, we need to know when the clock second changes to a new value. We then want to make a tone.

Edit the `code.py` file so it looks like this ...

```Python
last_second = 0
while True:
    # we randomize eye movement (front is always between other positions)
    eyes.update()
    lumos.update()

    if last_second != lumos.second:
        last_second = lumos.second
        lumos.tone(110, 0.05)
```

>>>
Remember that CircuitPython code uses spaces to indent lines and the indentation is important. The instructions within the loop are identified by their level of indentation. Similar, where there is an _if_ statement or another loop, the code statements for them are indented again.
>>>

The first line we added is ...

```Python
last_second = 0
```

This new variable will keep track of the value of the clock seconds.

The remaining lines we added are ...

```Python
    if last_second != lumos.second:
        last_second = lumos.second
        lumos.tone(110, 0.05)
```

These check if the current clock seconds value is the same as the one we stored in our variable. If not, then we update the variable and play a very short tone.

Save the file! The LumosRing should restart automatically and - after the time is set, the clock will buzz for each second.


## Samples

The pre-installed `code.py` is only a sample of what the LumosRing can be used for. There are more samples in the [examples folder](https://gitlab.com/bradanlane_cp/lumosring/-/tree/main/examples). To use any of those samples, copy the `.py` file to the `CIRCUITPY` device - naming it `code.py` to replace what is on the LumosRing.

### Default code.py

The pre-installed `code.py` is a simple clock. The clock renders tick marks to represent the location of hours around the clock. The 'hands' for hours, minutes, and seconds, are represented by red, green, and blue, respectively. 

If the `secrets.py` file has been added to `CIRCUITPY` and contains the necessary information for WiFi access and an Adafuit AIO account, the clock will automatically sync to internet time. As a backup for setting the clock, when the LumosRing is powered on, it will beep and ten wait for 15 seconds during which time, the left top button may be used to set the hours and the right top button may be used to set the minutes.

### chess.py example

When you copy `chess.py` to `CIRCUITPY` as `code.py` the LumosRing will run a simple chess match timer.

Use the top buttons to set the number of player minutes for the match. After 5 seconds with no input, the LumosRing will display 'GO'.

The left top button is for player 1 and the right top button is for player 2. Whichever player has the first move, presses their button. When they have completed the first move, the press they button again. After this initial move, each player presses their button once they have moved and the timer will automatically switch to the other player's time.

The game ends when a player's time runs out or a player forfeits by pressing both buttons together.

### metronome.py example

When you copy `metronome.py` to `CIRCUITPY` as `code.py` the LumosRing will begin a simple metronome.

Press the left button to decrease the metronome speed. Press the right button to increase the metronome speed. 

# glyphs.py example

When you copy `glyphs.py` to `CIRCUITPY` as `code.py` the LumosRing will display the sample 5x7 font.

The LumosRing 5x7 font includes symbols, numbers, upper case and lower case letters.

Press the left button to cycle through the font backwards. Press the right button to cycle through the font forwards.

# animations.py example

When you copy `animations.py` to `CIRCUITPY` as `code.py` the LumosRing uses the Adafruit neopixel libraries to render rainbows and sparkles.

The animations example does not use the `lumosring.py` library. It is provided to demonstrate using the LumosRing hardware with other neopixel modules.

Press the left button to reverse the direction of the ring animation. Press the right button to toggle the animation.


## LumosRing API

All of the configuration and capabilities are documented in the [**API**](API.md).

**Request:** If you use the LumosRing and find something missing from the API, you are encouraged to create an [_issue_](https://gitlab.com/bradanlane_cp/LumosRing/-/issues) in the gitlab project. Please describe what you are attempting to do and what is needed from the API. You are also free to make your own copy of the `lumosring.py` library.



