# code.py (LumosRing Animation example)
# Copyright: 2022 Bradan Lane STUDIO
#
# Open Source License: MIT

"""
This CircuitPython program demonstrates the basics of the LumosRing LEDs, buttons, and tone buzzer.

The LumosRing hardware connects via USB and provides:
    - 310 Neopixel LEDs
        - ring of 240 LEDs as 60 lines of 4 LEDs each
        - block of 10x7 LEDs as 7 lines of 10 LEDs each
    - tone buzzer
    - 2 buttons
    - a small mass storage device (used to store the code and related files)

The LumosRing is powered by a Lolin ESP32-S2 which include Wifi connectivity.

This example demonstrates using the popular Adafruit libraries directly without the use of the LumosRing library

Documentation: 
**Hardware:**
    * the LumosRing (available on Tindie)
      https://www.tindie.com/stores/bradanlane/
"""


import supervisor
import time
import board
import digitalio

import neopixel
from adafruit_led_animation import helper
from adafruit_led_animation.animation.rainbowcomet import RainbowComet
from adafruit_led_animation.animation.sparkle import Sparkle
from adafruit_led_animation.color import PURPLE

ring = neopixel.NeoPixel(board.IO16, 240, brightness=0.3, auto_write=False)
long = helper.PixelMap.horizontal_lines(ring, 4, 60, helper.horizontal_strip_gridmap(4, alternating=False))
wide = helper.PixelMap.vertical_lines(ring, 4, 60, helper.horizontal_strip_gridmap(4, alternating=False))

block = neopixel.NeoPixel(board.IO17, 240, brightness=0.15, auto_write=False)
sparkle = Sparkle(block, speed=0.1, color=PURPLE, num_sparkles=15)

comet = RainbowComet(long, speed=0.03, tail_length=15, ring=True, bounce=False)
waves = RainbowComet(wide, speed=0.06, tail_length=4, ring=False, bounce=False)

left  = digitalio.DigitalInOut(board.IO11)
right = digitalio.DigitalInOut(board.IO12)
left.direction  = digitalio.Direction.INPUT
right.direction = digitalio.Direction.INPUT
left.pull  = digitalio.Pull.UP
right.pull = digitalio.Pull.UP

left_was_pressed = (left.value == False)
right_was_pressed = (right.value == False)

animation = 0
while True:
    left_is_pressed = (left.value == False)
    right_is_pressed = (right.value == False)

    if animation:
        comet.animate()
    else:
        waves.animate()
    sparkle.animate()

    if (not left_was_pressed) and (left_is_pressed):
        # the left button has been pressed, we could do something ... like change direction
        comet.reverse = not comet.reverse
        waves.reverse = not waves.reverse
        block.fill(0)
        pass

    if (not right_was_pressed) and (right_is_pressed):
        # the right button has been pressed, we should do something ... like switch the animation
        animation = (animation + 1) % 2
        block.fill(0)
        pass

    left_was_pressed = left_is_pressed
    right_was_pressed = right_is_pressed

# end of while True loop
