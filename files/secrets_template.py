"""
    fill in the fields below and then rename this file to secrets.py
    copy the secrets.py file to the CIRCUITPY device


    To use AdafruitIO time server, include the following data within the curly braces below:

    'aio_username' : "name",
    'aio_key' : "keystring",
    'timezone' : "America/New_York", # http://worldtimeapi.org/timezones

    If you do not have an Adafruit IO key, follow this tutorial:
        https://learn.adafruit.com/adafruit-magtag/getting-the-date-time

       To use the WorldTimeAPI, you must include the timezone data within the curly braces below:

    'timezone' : "America/New_York", # http://worldtimeapi.org/timezones

    The AdafruiIO service will attempt to adjust for daylight savings time.
    The NTP service assumes UTC and supports a fixed offset which does not automatically adjust for DST.
    The WorldTimeAPI will provide local time with automatic adjustment for DST.

    To use an NTP server, it will use a default pool server
    or you may specific a pool server by including the following data within the curly braces below (suing your local offset):

    'ntp_server' : "pool.ntp.org",
    'ntp_offset' : -4,

"""

secrets = {
    'ssid' : 'ssid',
    'password' : 'password',
}