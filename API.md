# LumosRing

<img align="right" style="padding: 0 0 0 12px;" src="files/lumosring_512.jpg"/>

The LumosRing is a programmable neopixel 'display' consisting of a matrix ring of 240 LEDs (60x4) and a grid block of 70 LEDs (10x7).
It also has a tone buzzer and a pair of buttons to add interactions.

The LumosRing is a USB device running a CircuitPython program.

The LumosRing behavior and features are totally customizable using CircuitPython - an easy to use programming language. And if you really don't want to learn to program, that's OK too. The LumosRing is preprogrammed as a clock and there are other samples you can simply drag-and-drop to run.

The API provides easy-to-use configuration and operations capabilities.

If you are just starting out with the LumosRing, take a look at the [**README**](README.md) for an overview and some easy customization examples to get you started!

[[_TOC_]] 


## LumosRing API

The LumosRing library provides a set of _methods_ and _properties_ which you use in your `code.py` to customize the behavior of you LumosRing device. 

>>>
Methods and properties are terms used in CircuitPython programming. They are mentioned here for completeness but are not significant for customizing the LumosRing. The terms _method_ and _property_ are common to many programming languages. A _method_ is a capability of a library to perform an action. A _property_ is a piece of information of the library which may get looked at and some _properties_ may be changed. _(not all properties may be changed_)
>>>

What follows are the details of the library and what each function or attribute does as well as what inputs/outputs each has.

The LumosRing library provides access to its general operations, the buttons, LEDs, the buzzer, and has some convenient pre-defined keywords.

Here is a block diagram of the LumosRing library ...

<img align="center" style="width: 90%" src="files/block_diagram.png"/>

Before you can use the LumosRing library, you need to load it. Near the top of you `code.py` CircuitPython program, you will _import_ the library. The simplest method is with:

```python
from LumosRing import LumosRing   # REQUIRED: import the LumosRing library
```

### Operational Capabilities

#### LumosRing Constants

The LumosRing library begins with its operational capabilities. These include a number of pre-defined _properties_ to make programming easier and more readable. Each predefine property starts with `LumosRing.` and is then in all capital letters, as in `LumosRing.PRESSED` which is used to compare a key action to check if is pressed or released.

Here are all of the pre-defined properties:

| **Definition**    | **Description**                                      |
|:------------------|:--------------------------------------------------|
| `LumosRing.CLOCK`    | initialize the LumosRing as a clock |
| `LumosRing.TIMER`    | initialize the LumosRing as a timer |
| `LumosRing.COUNTDOWN`    | initialize the LumosRing as a countdown timer |
| `LumosRing.NOCLOCK`    | initialize the LumosRing with no timing features |
| `LumosRing.ONESECOND`  | value used for making delta time changes |
| `LumosRing.ONEMINUTE`  | value used for making delta time changes |
| `LumosRing.ONEHOUR`  | value used for making delta time changes |
| `LumosRing.LEFT`     | indicates a button event is from the left button |
| `LumosRing.RIGHT`    | indicates a button event is from the right button |
| `LumosRing.PRESSED`  | indicates the button has been pressed |
| `LumosRing.RELEASED` | indicates the button has been released |
| `LumosRing.RED`      | used for changing LED color |
| `LumosRing.GREEN`    | used for changing LED color |
| `LumosRing.ORANGE`   | used for changing LED color |
| `LumosRing.YELLOW`   | used for changing LED color |
| `LumosRing.BLUE`     | used for changing LED color |
| `LumosRing.MAGENTA`  | used for changing LED color |
| `LumosRing.PURPLE`   | used for changing LED color |
| `LumosRing.PINK`     | used for changing LED color |
| `LumosRing.CYAN`     | used for changing LED color |
| `LumosRing.WHITE`    | used for changing LED color |
| `LumosRing.GRAY`     | used for changing LED color |
| `LumosRing.DKGRAY`   | used for changing LED color |
| `LumosRing.BLACK`    | used for changing LED color _(black will turn an LED off)_ |


#### LumosRing Instance

Your `code.py` CircuitPython program needs to create an _instance_ of the LumosRing library. This provides access to all of the capabilities of the library. The simplest method is with:

```python
lumos = LumosRing(mode=LumosRing.CLOCK) # this is the default for creating your LumosRing object
```

#### LumosRing Settings

At anytime after you have created your LumosRing instance, you may set the default colors or brightness of the LEDs, change the operating mode, or change specific clock features. Here are examples of these settings:

```python
lumos.mode = LumosRing.TIMER
lumos.clock_background = LumosRing.BLACK
lumos.clock_colors = [0xff0000, 0x007f00, 0x0000ff, 0xff7f00, 0x0f0f0f]
lumos.brightness = 0.25
lumos.ticker = True
lumos.markers = True
```

Colors may be specified using the predefined choices _(see **Constants** above)_ or using hexadecimal values where the red, green, and blue portions are values from 00 to FF. For example, a mix of 50% red and 25% blue would be `0x7F003F`.

The `clock_colors` setting will take a list or a single value. The list is represented between square brackets `[` `]` and are color values for the hour indicator, minute indicator, second indicator, the _optional_ tick indicator, and the clock face markers. Any indicator may be hidden by providing 0x0000 _(BLACK)_ for that item. If a single value is provided, then all five items will be assigned the same color.

The `brightness` affects both the LEDs which make up the clock ring as well as the LEDs which form the block in the center. The value is between 0.01-0.75. The value is constrained to this range to insure the LumosRing does not draw excess power from the USB connection.

**CAUTION:** The `ticker` option requires the LEDs to update 60 times per second. To maintain this fast rate, the `markers` are not displayed. If you set both `ticker` and `markers` to `True`, the last setting will take precedence and the other setting will automatically be set to `False`.


#### LumosRing WiFi Setup

The LumosRing `CLOCK` mode will use an internet service to automatically set the time. The use of internet time requires the `secrets.py` file on the `CIRCUITPY` storage device to contain the proper WiFi setup.

There are two options for internet time integrated  into the library - AdafruitIO and NTP.
AdafruitIO required a free account. NTP does not require an account. 
AdafruitIO can automatically adjust for timezone and daylight savings time. NTP can apply a fixed offset.

The minimum information in the `secrets.py` file for WiFi access is:

``` JSON
secrets = {
    'ssid' : 'your_wifi_ssid',
    'password' : 'your_wifi_password',
}
```

AdafruitIO requires two additional data entires and an optional entry for setting the timezone.
The two `aio_*` settings are for Adafruit's IO services. These services are very easy to use.
Take a look at the [Adafruit IO](https://io.adafruit.com) website. 
If you do not yet have an account and would like to use the service to get the time, then create an account and then create a key.

``` JSON
secrets = {
    'ssid' : 'your_wifi_ssid',
    'password' : 'your_wifi_password',
    'aio_username' : 'your_aio_username',
    'aio_key' : 'your_aio_keystring',
    'timezone' : 'America/New_York', 
}
```

WorldTimeAPI requires one additional data entry for specifying the timezone.
The timezone is a text string and may be found from the [WorldTimeAPI website] (http://worldtimeapi.org/timezones).

``` JSON
secrets = {
    'ssid' : 'your_wifi_ssid',
    'password' : 'your_wifi_password',
    'timezone' : 'America/New_York', 
}
```

Fetching time from a default NTP server will occur automatically, if WiFi connectivity is available.
Optionally, the NTP server and a fixed timezone offset may be provided.

``` JSON
secrets = {
    'ssid' : 'your_wifi_ssid',
    'password' : 'your_wifi_password',
    'ntp_server' : 'pool.ntp.org',
    'ntp_offset' : -5,
}
```


#### LumosRing Execution

Most of the LumosRing execution is handled by `update()`. This needs to be called frequently. Don't worry about calling it too often.

The update is performed within your `code.py` `while True:` loop and looks like this ...

```python
    lumos.update() # this is very important so everything has a chance to check for new information
```

The `update()` performs all the necessary periodic tasks including getting new information about the joystick and its push-button, updating the screen, and updating the LEDs.


### LumosRing Clock and Timers

The LumosRing library simplifies building chrono-related projects like clocks, timers, and timing based games.

#### LumosRing Time

There are properties to get and set the current time using a `datetime` or using individual properties for hour, minute, and second. Here are several examples ...

```python
    current_time = lumos.time       # get the current time as a datetime 
    lumos.time  = current_time      # set the current time using a datetime 
    lumos.time += LumosRing.ONEHOUR # increment time using one of the constants
    current_hour = lumos.hour       # get the current hour 00..23
    current_minute = lumos.minute   # get the current minute 00..59
    current_second = lumos.second   # get the current second 00..59
    lumos.hour += 1                 # set the current hour one hour forward
    lumos.time = (12,30,0)          # set the time to 12:30 and zero seconds
```

#### LumosRing Timer

The LumosRing library provides support for `CLOCK` and also support for both `TIMER` and `COUNTDOWN`. These timer modes have two methods and one property ...

```python
    lumos.stop()
    lumos.start()

    if lumos.stopped:
      # do something
```

The `stop()` and `start()` methods control the running of the timer. The `stopped` property is either `True` or `False` for the timer actively running. For `COUNTDOWN` mode, the timer automatically stops when the time runs down to 00:00:00. Use the `stopped` property to determine if the time has run out.


### Buttons

The buttons are processed automatically and is independent of the `update()` operation.

The property `buttons_changed` is `True` if either button have been pressed or released. Each time `buttons_changed` is checked, the corresponding data available from the  `button` property. 

The `button` returns two values ...

```python
    button, action = lumos.button # get button data
```
The `button` value with be either `LumosRing.LEFT` or `LumosRing.RIGHT` and the `action` will be either `LumosRing.PRESSED` or `LumosRing.RELEASED`.

To process all button changes, you use a `while` loop ...

```python
    while lumos.buttons_changed:
        # lumos.button has the new key data
        button, action = lumos.button # get button data
        ...
        # do something with the button and action
```

### LEDs

There LumosRing ring and center block are made up of addressable RGB LEDs called neopixels. _(Adafruit provides [this introduction to](https://learn.adafruit.com/circuitpython-essentials/circuitpython-neopixel) neopixels.)_ 

The ring is made up of 240 neopixels organized as 60 columns of 4 LEDs each. You can think of the ring as a wide 60x4 rectangle. 

The LumosRing block in the middle is made up of 70 neopixels organized as 7 rows of 10 LEDs each.organized.

The LEDs are numbered starting with 0. The `0` row of LEDs is top dead center and the `0` LED in that row is the one on the inside of the circle. The `3` LED is on the outside of the circle.

The LumosRing library uses the ring to render chrono-related data. The library treats the block as a matrix for bitmaps, letters, or numbers.

**Note:** The LumosRing library is not required to use the LumosRing board. The library aids with chrono-related projects. For more artistic control and completely custom applications, the LumosRing board may be programmed with Adafruit and third-party libraries. It is also possible to program the LumosRing board with Arduino or Espressif but those are left as an exercise for th reader. 



The default LED color is set using the `color` property _(see `color` in 'LumosRing Settings' above)_

An LED color is changed using `led(num, color)`. Color may be one of the predefined colors _(see `LumosRing Constants` above)_ or a custom color. A custom color is created using parentheses around three numbers, representing red, green, and blue, e.g. `(162, 0, 37)` is crimson.

All of the LEDs may be changed at once using `leds(color)`.

The brightness of the LEDs is controlled by the `brightness` property.

```python
    lumos.leds(LumosRing.BLUE)                            # set all LEDs in the ring to blue
    lumos.led((29, 3), LumosRing.RED)                     # set the forth LED of the 30th column (6-o'clock position)
    lumos.line(29, LumosRing.GREEN)                       # set all of the LEDs of the 30th column (6-o'clock position)
    lumos.line((29, 2), LumosRing.GREEN)                  # set the first 3 LEDs of the 30th column (6-o'clock position)
    lumos.bitmap(image, 0x1F0000, 0x0F0F0F)               # set all of the LEDs in the block using a 1-bit per pixel image
    lumos.digits(num, LumosRing.BLUE)                     # render a 1 or 2 digit number in the block
    lumos.letters(text, LumosRing.GREEN, LumosRing.GRAY)  # render up to 2 characters in the block
```

**Note:** Remember the numbering starts as `0`.

Colors may be provided using the predefined colors listed in the **Constants** section above or as a hexadecimal number. The methods for adding content to the block of LEDs in the center of the LumosRing board have a default background color of `BLACK`. You may omit providing a background color if you want `BLACK`.

The digits and letters are rendered using the `5x7.pcf` font from the `font/` folder on the `CIRCUITPY` storage device.


### Buzzer

The LumosRing has a very simple buzzer. It is able to produce a simple tone. It will nto play sound files.

The buzzer may play a tone for a period of time or it may produce a tone until instructed to stop.

The tone is given as a frequency from 0 to 16K. In real terms, a value below 8K is recommended.

Use `tone(frequency, duration)` to play a tone for a fixed amount of time. The duration is in seconds and may be a decimal number, e.g. `0.5` is one half second.

**Caution:** The `tone()` will only finish after `duration`. Do not use a long duration if you are updating the LEDs frequently because these tasks will wait until `tone()` completes.

An alternative to `tone()` is to use `toneon(frequency)` and `toneoff()`.

Here is an example of playing a tone while the right button is pressed and stopping the tone when the right button is released ...

```python
while True:
    while lumos.buttons_changed:
        button, action = lumos.button
        if (button == LumosRing.RIGHT):
          if action:
                lumos.toneon(440) # 'A' on the music scale
          else:
              lumos.toneoff()
```


### Direct Access

The LumosRing hardware may be used without the LumosRing library. Here are the CircuitPython board pins on the Lolin Mini ESP32-S2 and their assignments.

| Board Pin       | Usage                                      |
|:-----------------|--------------------------------------------------|
| **Board.IO11**   | Left button (open HIGH) |
| **Board.IO12**   | Right button (open HIGH) |
| **Board.IO16**   | Neopixels of the ring (total of 240) |
| **Board.IO17**   | Neopixels of the block (total of 70) |
| **Board.IO18**   | Buzzer |
| **Board.IO9**   | Buzzer EN (HIGH for enabled, LOW for disabled) |


**Tip:** Check out the [examples](examples/) for a examples of many of the API capabilities.
The `metronome` example is a good example of using the buttons.
The `chess` timer example is a good example of a countdown timer and for using the buttons.
The `glyphs` example shows all of the upper and lower case letters.
The `animation` example uses the LumosRing board without the LumosRing library.

To load and example onto the LumosRing, copy the file to `CIRCUITPY` with the destination name `code.py`

